# Services

Services are one of the main building blocks of Vehiculum. So keeping services sane, will keep everyone in the dev-team a little more sane in the future. That's the reason on this document, I'll try to cover a general structure, how a service is called, what should be a service, which services should go where as much as possible.

## Service Structure

Every service class should be done in **UNIX philosophy**, a service shoud be responsible for *one thing and one thing only*. Of course there will be exceptions to this rule, but at least try not to stray away from the idea.

## About Commands

Services are commands actually represent same exact structure. There are very minor differences between them. Current development mostly takes place under **Services**.
 