# Robots.txt

Web Robots (also known as Web Wanderers, Crawlers, or Spiders), are programs that traverse the Web automatically. Search engines such as Google use them to index the web content, spammers use them to scan for email addresses, and they have many other uses.

### Instructions for uploading new robots.txt

- Login via https://vehiculum.signin.aws.amazon.com/console
    username: marketing
    password: Z7IVHwtnmW3p
- Locate S3 via AWS Console UI(it should be listed under the search box), you can just write down S3 to the input box on top. Click on it.
- Choose 'vehiculum-marketing' bucket from buckets list
- Click Upload, choose new robots.txt file, and click upload(don't click on next)
- Done, new robots.txt file is online.

Stuff to consider when writing robots.txt file.
- It has a very basic structure that let's you allow or disallow any url's.
- If an url is not DISALLOWed, then it's ALLOWed by default. So there is no need to write
ALLOW commands for not DISALLOWed urls.
- If an url is DISALLOWed, sub url's can still be ALLOWed with ALLOW commands. For example,
Disallow: /suche
Allow: /suche/start
Will disable any request to /suche url other then /suche/start url.
- The following lines must be in robots.txt file no matter what for security and other reasons
User-agent: *
Disallow: /admin/
Disallow: /dealer/
Disallow: /customer/
Disallow: /anfrageprozess/
Disallow: /uploads/car_spec/
Sitemap: https://www.vehiculum.de/sitemaps/index-sitemap.xml