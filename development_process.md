## Development Process

Development process is heavily based on,
 - Bitbucket
 - JIRA
 - Codeship.

 The following parts will have explanations of development steps.


### 1 -  Ticket Creation

The process starts with **ticket creation on *JIRA***. Created ticket should include detailed information about what the expected outcome is, what possible ways of implementation is possible, all the findings regarding to problem up to that point, also having a good-representative name is important.

### 2 - Branching

After the ticket gets assigned to a developer, the branching step starts. Initial action is to create a branc from *JIRA* about this issue. This action can be done via the button **Create Branch** right bottom panel of the issue.

When branch is completed the development can take place on that branch. Try to commit as small as possible while trying to maintain a runnable state on each commit.

### 3 - Codeship

Every time you push code to your new branch, codeship will pull the changes and test them agains a huge suite of tests. This will take around 10 minutes, so you can grab a coffee in the meantime.

Codeship is divided into two main test pipelines for faster test resutls,

  - Pipeline 1 : A pipeline that runs **RuboCop** and backend tests.
  - Pipeline 2 : A pipeline that mainly runs integration tests(which are slower usually)

So at this stage, if both pipelines are not green, this means you have some issues. Try to solve them, and retry running the pipelines.

**BUT**, because the pipelines take too long, and there is only one branch processed at once, please try to run **RuboCop** locally to not clog the pipelines with repepated RuboCop offenses.

### 4 - Pull Request

Third step is the pull request. When done with the issue at hand, after pushing all the changes to correct branch on **Bitbucket**, start creating a pull request. The pull requests **target should be *master***. Initially add as many developers as possible to the pull requests for at least 2 very good reasons,

  - Exposure. There are people focused on different aspects of the project, with different ideas. Try to expose your code to as many as possible to create a mindset for yourself for the future.
  - The project has many different aspecs, and most developers are focused on some parts of the monolith. So it's not really possible to figure out the correct people for task at hand initially.

### 4.5 - Pull Request

Pull requests at Vehiculum is not for only simple code reviews. There will be a lot of discussions, especially on your first commits. Don't forget these are not personal, nor directed at you. It's all about creating the next big thing with **minimal technical-debt**.

When everyone is happy with the PR, every PR needs at least 1 approval. After the approval is granted from one of the other devs, don't directly merge your code into master, there is one more step involved.

### 5 - Staging

Just before merging the PR to master branch, there is one more step to take. This step is simply deploying your code to **staging** environment, having **someone from QA** or the **Product Owner** to take a look at your code. This step is quite important for frontend related issues, for backend-only PR's it might be enough to just write some good specs.

Deployment to staging is a little bit involved, so bear with me.

  1. First step is to switch to staging branch, pull the staging branch(there might be another developer deployed to staging since your last pull), and merge your changes into the staging branch. There might be some conflicts and even some more issues regarding to this step.
  - If your changes didn't created any conflicts on the branch, that good news, you can continue with the next step 2.
  - If your changes created some conflicts, the best course of action is first looking at the specific files, and trying to understand what the others pushed to staging. Ask the latest commit's owner on the staging branch. The largest problem at this stage is, **if someone else is testing the staging branch at the moment** the next step might ruin their day(more like next 15 minutes, but still), so make sure you communicate well at this stage :). Because what you need to do is, reset the staging to master branch. Then merge your branch again.
    ```
    git reset --hard origin/master
    ```
    This will **throw away all the committed changes** on staging, so it's nice to also notify the **#dev** channel on slack about this.
  2. After merging your changes to staging branch, first step is to push them to **origin/staging**.
  3. Now if all the changes are pushed to the **origin/staging** branch the next step is to deploy to heroku. 
      ```
      bin/deploy staging
      ```
