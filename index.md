# Vehiculum 

Vehiculum is a project that manages vehicle leasing processes between clients and suplliers by acting as a negotiator. 
The project is mostly have a monolithic structure with some microservice-like sub systems.

## High-Level Project Structure

Vehiculum project utilizes Services(or Commands) pattern for most actions. Which simply means there are a lot of transaction-like operations defined under the classes **ApplicationService**(app/services) or **BaseCommand**(app/commands). 

These operations are called from either controllers, models or background-jobs. Vehiculum also utilizes background-jobs as much as possible.

Models structure of Vehiculum is rather complicated, but most of the operations regarding to database, are done on a sub-group, which this documentation is going to cover at some point.

The project currently hosted on **heroku**. Building of the development environment starts just like any other RoR application, but diverts after database creation. Instead of seeding the database, we have a system that let's us pull all the data from production database and anonimyze. This creates a nice environment because most of the data on the development machines are represantative of the production system.

There are some guidelines regarding how the code should structured and some best practices which can be found in style_guide.md. Styling is quite important for a porject at this scope, so I would definetly suggest going over it at least once. 

