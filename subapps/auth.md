# SSO @ Vehiculum

@ Vehiculum, we've decided to divide our overgrown monolith application into smaller sub-application(subapp). One of the biggest problems in this area is authentication-authorization mechanisms on different subapps. To achieve this we've decided moving on with the simplest solution of cookie sharing.

## Ideal Target

Our target at subapp structure is to remove everything from vehiculum application except user authentication and authorization. Except user authentication and authorization, every part of the applcation should be moved into it's own namespace, share minimum amount of resources with other subapps, and should be able to live on it's own without any other dependency.  

## Cookie Sharing

Cookie sharing is a method that we use to share cookies between subapps. The session cookie from rails is shared under the domain .vehiculum.de, which means we can actually deploy applications like 'subapp.vehiculum.de' and we will still be able to access the .vehiculum.de cookie from the subdomain.

## Session Sharing

Session sharing is the target we are trying to achieve with cookie sharing. The idea is to keep a connection to main vehiculum application's database, with the same model structure from main application. This requires duplication of the models in our application into subapps.

To achieve this in a more straigthforward way, the application's models will be divided into different gems. The first gem is going to only include authentication process, other gems might depend on this gem to be able to work. 

### gem vehiculum-authentication

vehiculum-authentication gem is going to be the first gem on the field, which will only carry over the user model and related functionality to the subapp. 

## gem vehiculum-authorization

vehiculum-authorization gem is going to be the second gem on the field, which will only carry over rolify dependent stuff. 

## gem vehiculum-user-management
