# What is Capybara? What is PhantomJS? What is Poltergeist?


##What does each component do?
### Capybara lets you automate testing of web apps.

*Capybara* is a testing system, designed to be able to use a web browser like a real user would, but automatically. It is used to help find errors in "web apps", simulating the way a human tester would find them.

*PhantomJS* is a web browser, using the Webkit browser engine (the core part of Safari). It runs from the command-line, without showing a browser window.

*Poltergeist* allows Capybara to interact with PhantomJS. Capybara has a standardized way to do tests, which Poltergeist is designed to understand. Poltergeist also knows how to translate these tests to a way that PhantomJS will understand.

All together: Capybara tells Poltergeist what to do, Poltergeist tells PhantomJS what to do. This way, our site gets tested in a real browser (somewhat like Safari), automatically.

Capybara can also be used with Cucumber and Rspec, more on that below.

PhantomJS is the web browser at the core.
PhantomJS is programmed to run JavaScript files (i.e. have a full WebKit browser execute your JavaScript files) and return some output so you know how it went. Considering how completely web pages can be navigated using lines of JavaScript code, this ends up being very powerful. See this guide for details.

(By the way: PhantomJS includes a bunch of functions of its own, accessible by JavaScript, such as a function that takes a screenshot of the current web page. e.g.: page.render('output.png').1 We aren't using PhantomJS directly, so knowing about these functions isn't directly useful to Refuge Restrooms.)

### Poltergeist makes PhantomJS do stuff.
Not much to say here. Capybara defines some capabilities it needs from an application, in order for Capybara to run any tests on said application. Poltergeist understands those capabilities... and can generate JavaScript commands for PhantomJS to run. Poltergeist's job is to ensure Capybara and PhantomJS can talk to one-another and function properly. More info here: https://github.com/teampoltergeist/poltergeist

### Capybara is the testing framework.
Capybara has one language you can write to do many standardized actions on a web page. (It can't do any of those actions by itself; it relies on other software to provide a working web browser, and if that browser doesn't "speak" Capybara's language, some software will be needed to translate between Capybara and the browser. Lots of software has been made compatible with Capybara to provide a working browser to test with. The big ones are RackTest, Selenium, Capybara-webkit, and Poltergeist.2)

### How do we use Capybara?
Capybara has a "language" of its own, but it is written in Ruby. It is a series of terms that have a special meaning to Capybara, and which are all about working with a web browser. (Capybara's developers call this a "Domain-Specific Language" or "DSL".)

These functions are focused on things a user would want to do, like clicking on stuff,3 for example:

click_link('id-of-link')
click_button('save')
But also can do things that are more technical, like finding HTML elements by their CSS properties...4

find_link(class: ['some_class', 'some_other_class'])
Or asserting that a certain feature of the page has certain qualities:5

expect(find('#sidebar').find('h1')).to have_content('Something')
