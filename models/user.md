# User
User model, while being one of the core models in the Vehiculum system, is pretty shallow in terms of complexity if you've worked with **devise** before. If not, no problem, devise is pretty well covered on their documentation, please go ahead and take a look at their documentation.
> [https://github.com/plataformatec/devise/wiki]

---
> User model doesn't really have a direct real word represantation. There are other models, which represent real people. You can think of user model as an id card for people. While they all have different jobs to achieve, everyone gets an id card.